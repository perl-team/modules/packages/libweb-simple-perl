libweb-simple-perl (0.033-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 22:26:14 +0100

libweb-simple-perl (0.033-1) unstable; urgency=medium

  * Import upstream version 0.033.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.0.0.

 -- gregor herrmann <gregoa@debian.org>  Thu, 22 Jun 2017 18:13:44 +0200

libweb-simple-perl (0.032-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.032
  * debian/copyright: Update years.
  * debian/patches/spelling.patch: Remove, applied upstream.

 -- Angel Abad <angel@debian.org>  Wed, 16 Nov 2016 10:03:19 +0100

libweb-simple-perl (0.031-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Add (build) dependency on libhttp-body-perl. (Closes: #842462)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.8.
  * Drop unneeded version constraints which are already fulfilled in
    oldstable.
  * Add a patch to fix spelling mistakes in the POD.

 -- gregor herrmann <gregoa@debian.org>  Sat, 29 Oct 2016 14:59:49 +0200

libweb-simple-perl (0.031-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata.
  * Import upstream version 0.031.
  * Update years of packaging copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Mon, 26 Oct 2015 20:09:08 +0100

libweb-simple-perl (0.030-1) unstable; urgency=medium

  * New upstream releases 0.029, 0.030.

 -- gregor herrmann <gregoa@debian.org>  Tue, 12 Aug 2014 17:52:09 +0200

libweb-simple-perl (0.028-1) unstable; urgency=medium

  * New upstream release.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 12 Jul 2014 21:18:59 +0200

libweb-simple-perl (0.024-1) unstable; urgency=medium

  * New upstream release.
  * Drop pod-whatis.patch, fixed upstream.

 -- gregor herrmann <gregoa@debian.org>  Fri, 04 Jul 2014 12:23:55 +0200

libweb-simple-perl (0.023-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream releases 0.021, 0.023.
  * Update debian/copyright.
    Add new copyright holders, bump years of packaging copyright.
  * Replace pod_fragment.patch with an override in debian/rules.
  * Add patch to fix POD (missing whatis entry).
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 24 May 2014 14:26:42 +0200

libweb-simple-perl (0.020-1) unstable; urgency=low

  * Team upload.

  [ Nuno Carvalho ]
  * New upstream release.

  [ gregor herrmann ]
  * Add new upstream copyright holder.

 -- Nuno Carvalho <smash@cpan.org>  Tue, 07 Aug 2012 19:02:25 +0100

libweb-simple-perl (0.016-1) unstable; urgency=low

  * Imported Upstream version 0.016

 -- Fabrizio Regalli <fabreg@fabreg.it>  Mon, 14 May 2012 12:16:47 +0200

libweb-simple-perl (0.014-1) unstable; urgency=low

  * Imported Upstream version 0.014
  * debian/copyright: add year 2012 to debian/* files

 -- Fabrizio Regalli <fabreg@fabreg.it>  Fri, 04 May 2012 00:15:47 +0200

libweb-simple-perl (0.013-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.3 (no changes).
  * debian/copyright: update to Copyright-Format 1.0.

 -- gregor herrmann <gregoa@debian.org>  Thu, 05 Apr 2012 19:27:32 +0200

libweb-simple-perl (0.012-1) unstable; urgency=low

  * New upstream release.
  * Update list of copyright holders and years.

 -- gregor herrmann <gregoa@debian.org>  Tue, 07 Feb 2012 18:42:07 +0100

libweb-simple-perl (0.011-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 23 Dec 2011 14:02:15 +0100

libweb-simple-perl (0.010-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: add additional upstream copyright holders.
  * Remove fix-pod.patch, applied upstream.

 -- gregor herrmann <gregoa@debian.org>  Fri, 07 Oct 2011 14:18:49 +0200

libweb-simple-perl (0.009-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream release 0.004.
  * Convert to source format 3.0 (quilt). Remove quilt framework.
  * Refresh debian/copyright.
  * Set Standards-Version to 3.9.0 (no changes).
  * Update build and runtime dependencies.
  * Refresh pod_fragment.patch.
  * Remove repacking framework; the files are not included anymore.
  * Remove version from perl (build) dependency.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Fabrizio Regalli ]
  * New upstream release 0.008
  * Upgrade debhelper to (>= 8)
  * Update d/compat to 8
  * Added myself to Uploaders and Copyright
  * Added libweb-simple-perl.docs file
  * Bump to 3.9.2 Standards-Version
  * Removed 'GNU/Linux' from copyright
  * Added libdata-dumper-concise-perl (>= 2.020), libmoo-perl,
    libwarnings-illegalproto-perl as B-D

  [ gregor herrmann ]
  * New upstream release 0.009.
  * debian/copyright: update years of upstream and packaging copyright,
    remove information about removed third-party files.
  * Add a patch to fix a pod2man error.

 -- gregor herrmann <gregoa@debian.org>  Tue, 04 Oct 2011 18:15:55 +0200

libweb-simple-perl (0.002+dfsg-1) unstable; urgency=low

  * Initial release (closes: #559732).

 -- gregor herrmann <gregoa@debian.org>  Sun, 06 Dec 2009 19:38:57 +0100
